---
layout: page
title: Página de buttons
description: Aqui você vai encontrar uns buttons legais de blogues de amigos :3
permalink: /buttons/
---

Aqui tem uns buttons legais de uns blogues que eu gosto, se quiser que o seu apareça aqui, manda uma mensagem no mastodon :3 

Só precisa se atentar com o tamanho. Esse do Groselhas tem 88x31 pixels, não precisa ser igualzinho, mas é bacana pra ficar padronizado. 

Dito isso, criem os seus próprios buttons, deixem eles bem coloridinhos!

Aproveite! :3

---


<div class="buttons-grid">

<img class="button-img pixel-art" src="/assets/img/buttons/buttongroselhas.gif">
<img class="button-img pixel-art" src="/assets/img/buttons/buttonpuroosso.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">
<img class="button-img pixel-art" src="/assets/img/buttons/samplebutton.png">

</div>
