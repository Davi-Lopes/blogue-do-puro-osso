---
layout: page
title: Sobre
permalink: /sobre/
---

Ok, tentando de novo....

Olá! seja muuuuito bem vindo ao meu mais novo blog! (perdi o projeto dos outros, e isso talvez vire um post aqui xD).

Aqui você vai encontrar conteúdo sobre:

- Linux
- Programação
- Cultura Pop
- Cultura Gay
- E muito, mas muito rock B)

Aqui é um lugar onde vou compartilhar meus trabalhos, pensamentos e opiniões, tudo com muito amor e carinho :3 

Este blogue foi feito usando o Jekyll, uma ferramenta de geração de páginas estáticas, pensada principalmente pra blogues.

Já tentei usar outras, mas todas me deram problemas, essa foi a mais tranquila de usar, de longe...

Aqui é tudo open source, então você vai encontrar o código fonte dessa budega na minha conta do gitlab, é só dar uma olhada na aba _social_.

bom, é isso que eu tinha pra dizer, divirta-se! :)
