---
layout: page
title: Social
permalink: /social/
---

## Gitlab:
[gitlab.com](https://gitlab.com/Davi-Lopes)

## Mastodon:
<a rel="me" href="https://bolha.one/@PuroOsso">bolha.one</a>

## Lemmy:
[lemmy.eco.br](https://lemmy.eco.br/u/Sou_Puro_Osso)
