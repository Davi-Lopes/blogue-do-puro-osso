---
layout: post
title:  "Criando uma distribuição linux"
date:   2024-02-24
categories: Linux
description: Bom, este é o primeiro post do meu blogue, espero que goste :)
---


Acordei hoje com uma puta vontade de criar uma distribuição Linux, mas, uma distro diferente de todas as outras (como toda distro se diz ser). Não tenho certeza de onde veio essa ideia maluca, mas, [esse vídeo](https://redirect.invidious.io/watch?v=brIQSA8FtDo) do querido Fabio Akita me inspirou de alguma forma. 


É claro, não vou criar uma distro diferente de todas as outras, existem milhares, provavelmente alguém já teve a mesma ideia que eu (ideia que na verdade eu nem tenho ainda), mas bem, fazer isso seria no mínimo interessante, e com certeza seria um belo de aprendizado pra mim, usuário médio de Linux. E claro, conhecimento no código aberto, não pode ficar parado, então, eu iniciei este blogue (sim, blogue, existe palavra pra isso em português).

No momento que estou escrevendo isso, eu nem sequer tenho ideia do nome, nem da estética, ferramentas, nem de caralha nenhuma sobre como vai se parecer isso, mas, eu vou fazer (espero).
Eu inclusive, espero que isso seja uma inspiração para você também criar o seu próprio blogue, sério, é muito mais interessante do que ficar escrevendo “mini postagens” como você faz no twitter. Inclusive, o conteúdo é seu, pode postar o que quiser nele, sem medo de ser banido da plataforma por falar o nome de plataformas. Provavelmente este blogue provavelmente vai ganhar uma postagem só sobre isso.

Mas bem, voltando ao assunto…

Pra começar, nada melhor do que entender o que é uma distribuição Linux, não é? Bom, vamos dividir os tópicos...

1. O que é Linux.
2. O que é um kernel?
3. O que é GNU/Linux

Bom, isso é simples, ele é um kernel. Posso passar pra o próximo tópico? Nah! 
Ele foi criado do zero, por Linus Torwalds, inspirado no sistema Unix (Linus + Unix == Linux), lá no ano de 1991, aparentemente como um hobby, ou um projeto sem a intenção de escalar e virar um sistema que seria mundialmente adotado por servidores e alguns nerdolas em seus desktops.
Mas ok, o que é um kernel?

Pausa pra um banho, calor dos infernos >:/
De volta ao assunto…

Kernel é um componente do sistema operacional, que basicamente controla… tudo! Essa parte do SO (sistema operacional) está sempre carregada na memória RAM, porque precisa ser de fácil e rápido acesso. Existem outros Kernels fora o do Linux, como os que vem nos sistemas BSD, por isso, não é possível rodar aplicativos Linux em BSD, porque o kernel é diferente, e as funções C (Linguagem de programação) são diferentes nos dois kernels.

Mas é claro, o kernel não é o OS por si só, nele não tem os aplicativos que você usaria, por exemplo, então, de onde vem os programas de Linux? Aí temos a explicação do porque muitas pessoas se referem a Linux como “GNU/Linux”, ele é um sistema operacional, que adota o kernel Linux, e basicamente, a maioria das distros que você utiliza, são GNU/Linux, Ubuntu, Debian, Arch Linux, enfim, a grande maioria. Tem o Alpine, que é escrita do zero, mas não é GNU/Linux.

E o que aprendemos com isso? Que uma distribuição Linux é basicamente pegar os aplicativos do GNU e botar o kernel do Linux dentro. Se formos criar uma distro do zero, é legal que já temos literalmente um livro explicando como isso funciona [aqui](https://www.linuxfromscratch.org/).

Mas, por hora, prefiro inciar com base em uma já existente, vai me poupar tempo :)
Eu escolhi o Debian, é estável, simples, e todo mundo gosta. 

Bom, pra esse post, acho que é isso, eu já molhei os pés nisso modificando uma iso de Debian, no próximo post eu mostro o que descobri, até lá! :)

