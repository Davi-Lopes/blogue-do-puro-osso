---
layout: post
title: Introdução ao meu curso de lógica de programação
description: Um post introdutório sobre um curso de lógica de programação que estou fazendo!
categories: Curso de programação
---
Olá! Este é a primeira postagem do meu cursinho de lógica de programação!

Aqui você vai encontrar bons artigos sobre o assunto, dês do mais básico até o mais avançado! Por mais irônico que seja, vamos escrever pouco ou nenhum código da maioria das aulas, isto porquê, programar é muito mais complicado do quê escrever letrinha colorida da tela. Muitos programadores hoje nem sequer sabem como fazer contas com valores hexadecimais, nem o que é uma porta lógica, e um dos motivos, é porquê os falta estudos aprofundados em lógica de programação.

 Além de quê, precisamos matar o mito de que programação é algo fácil, que todo mundo pode aprender em poucos meses, o que está longe de ser verdade (menos a parte de que todo mundo pode aprender), meses é só o básico, algorítimos bobos de rotinas simples. E não problema ser assim, essas coisas são complicadas mesmo, sério, se o seu mentor tá te falando que dá pra aprender menos de uns bons anos, sai fora, é furada.

Mas não se preocupe, como eu falei, é demorado mesmo, então, se você tá com esse sonho de querer ser um programador porquê ouviu o vampiro hipnótico falando de curso grátis pra virar um programador de sucesso e rico, pode esquecer. Principalmente se for um daqueles cursos de 3 aulas, nem olha, é só 3 horas de bullshit. Mesmo aqui, eu só vou tentar passar vários conteúdos legais e importantes para se aprender, e, óbvio, não vai caber tuuuudo, nem vai te substituir uma faculdade.

O que eu vou falar aqui no curso? Olha só:

1. Código binário
2. Operações bitwise
    1. Shift right
    2. Shift left
3. Portas lógicas
    1. AND
    2. OR
    3. NOR
    4. XOR
    5. XNOR
4. Tipos de dados
    1. Integer
    2. Char
    3. String
    4. Float
    5. Boolean
    6. Palavras reservadas
    7. Imutáveis
5. Variáveis
    1. Globais
    2. Locais
    3. Constantes
6. Operadores
    1. Operadores algébricos
    2. Operadores de condição
7. Estruturas
    1. Estruturas de condição
    2. Estruturas de repetição
8. Input e Output
9. Funções
    1. Argumentos
    2. Retornos

Bastante coisa, né? Nem tanto, isso aí dá pra cobrir em poucos artigos, considerando que Cada subtópico vai ser tratado dentro de um mesmo artigo. E, de novo, obviamente isso não cobre nem metade do assunto de lógica de programação, chega nem perto. Talvez eu continue adicionando novos assuntos a medida do tempo, mas por enquanto, isto deve servir para que você, leitor continue pesquisando.

Eu vou ao máximo tentar criar programinhas simples para vocês treinarem o assunto, eles serão acessíveis pelo meu blogue, e vão estar sob a licença GPL-3. Praticar o assunto é importante, então, se você é um garoto menor de idade que ainda não tem um monte de responsabilidades grudadas na suas pernas, aproveitem o tempo livre pra isso, sério, daqui uns anos vocês não vão ter tempo pra nada.

E bom, por enquanto é isso, neste momento eu já vou estar escrevendo o artigo sobre Código binário, espero muito muito muito que vocês gostem c:
