---
layout: post
title: Introdução a código binário
description: Falando sobre binários e tal
categories: Curso de programação
---

Para dar início ao conteúdo de lógica de programação, primeiramente, devemos entender como usar o código binário, entender como representar números nele, e tudo mais… É bem provável que você já tenha ouvido falar nisso, e, deve saber que usamos eles pra representar números, letras, e afins. Precisamos entender ele, pois os computadores se comunicam a partir distro, e não existe uma forma mais complexa de representar coisas (geralmente “coisas” são números). Isto porque, computadores são maquinas que só entendem “ligado” e “desligado”, usando interruptores, válvulas, tanto faz, todos eles ainda usam este sistema de ligar de desligar coisas.

Mesmo hoje, que nós programadores não escrevemos mais código binário, ainda usamos lógica binária, de verdadeiro, ou falso. Estão notando um padrão? É sempre algo inverso a outro, duas constantes. Isso vai ser muuuuito importante mais pra frente. Bom, pra começar, vamos entender algumas unidades de informação.

## BIT
O bit, é a menor unidade, ele só é capaz de representar 1 ou 0. 

## Byte
O byte, representa 8 bits, por exemplo: 01011011. Ele é bastante usado para representar armazenamento de forma precisa. Também pode ser chamado de octeto.

<br>

Tem mais, mas por enquanto, vamos usar só esses dois. Que tal fazermos umas conversões?

0 = 0  
1 = 1  

Por enquanto, bem simples e óbvio, né? Mas e que tal…

10 = 2  
11 = 3  

Ok, agora parece mais difícil, né? Mas relaxa! Na verdade é bem fácil, olha só:

![](/assets/img/curso_logica_src/explicacao_binario.png)

Como podem ver, é realmente bem simples, começando da direita, vamos pegando a posição de cada algarismo, e adicionando a base 2. Por exemplo, o 5° dígito (que é um 0), fica: 2⁵, que é 32.  
Fazendo isso com todos eles, podemos ignorar todos os zeros, e somar o que sobrar. No nosso exemplo, 64 + 4 + 2 +1, que é 71, ou seja, 01000111 = 71. E pra facilitar, podemos tirar o zero a esquerda que sobrou, daí fica: 1000111 = 71.

Viu? É super fácil!

Mas… e se quisermos fazer o caminho inverso, ou seja, converter de decimal para binário?

Também não é tão difícil, vamos tentar com 77!

<br>

Seguimos a mesma lógica de converter binário em decimal, mas, dessa vez, da esquerda para a direita.

<pre>
128 64 32 16 8 4 2 1
--------------------
0   0  0  0  0 0 0 0
</pre>

Vejamos, se colocarmos o 1 no 128, vai ser maior que 77, então, passamos pro próximo.
E então estamos no 64, que é menor que 77, então ele vai receber o 1.

<pre>
128 64 32 16 8 4 2 1
--------------------
0   1  0  0  0 0 0 0
</pre>

Se colocarmos o 1 no 32, vamos ter 64 + 32 = 96, que é maior que 77, então vamos para o próximo.

Talvez o 16? 64 + 16 = 80, não, ainda é maior que 77...

E chegamos no 8, 64 + 8 = 72, ótimo!

<pre>
128 64 32 16 8 4 2 1
--------------------
0   1  0  0  1 0 0 0
</pre>

Ainda falta 5 para chegar no 77, e o próximo é 4, ou seja: 72 + 4 = 76

Agora só falta 1, então vai ficar assim:

<pre>
128 64 32 16 8 4 2 1
--------------------
0   1  0  0  1 1 0 1
</pre>

E então matamos! 01001101 = 77, super simples, não é?

<br>

Agora... seria legal dar uma treinada, né? Bom, eu criei um site bem simples pra te ajudar com isso! Acesse: [adivinhe-em-binario.puro-osso.dev.br](https://adivinhe-em-binario.puro-osso.dev.br).  
O jogo é simples, você recebe um código binário, e deve dizer qual é o número decimal equivalente a ele. Você tem 5 jogadas, se acertar, mantém uma jogada, e ganha pontos. Se errar, perde uma jogada. Tira um print do seu resultado e me marca no Mastodon! (procura na aba “social” do meu blogue), que eu vou ficar dando boost c:

Enfim, continuando no assunto…

# Matemática

Outra coisa importante, é saber fazer operações algébricas com os números, o que também é suuuper simples!

Um das formas mais simples, é simplesmente traduzindo os binários, e executando a função algébrica nos decimais, assim:


111 = 7  
010 = 2  

7+2 = 9  
9 = 1001  

Ou seja, 111 + 010 = 1001, simples, né? Você pode fazer isso pra qualquer coisa, subtração, divisão e ETC. Porém, só saber fazer assim não é útil pra entendermos os próximos assuntos, então, vamos ver como fazer somas e subtrações sem converter os binários (divisões e multiplicações entram no próximo assunto).

# Somando

![](/assets/img/curso_logica_src/explicacao_soma_binario.png)

Como podemos ver, é exatamente a mesma coisa que somas feitas em decimal.

Na coluna 0 (da direita para a esquerda), temos 1 + 1, que em decimal, daria 2, mas em binário, dá 10 (um zero, não dez), este 1 a esquerda, está sobrando, e ele vai “subir” como você faria em uma conta normal, isso se chama “carry”.

Na coluna 1, temos 1+0+1, que como já sabemos, que também dá 2, ou seja, 10, e subimos mais um.

Na coluna 2, temos 1+1+1, que dá 111, então deixamos 1, e subimos 1.

E por fim, na coluna 2, temos 1+0+0, que dá 1.

Viu como é simples? Realmente, você faz como faria em qualquer outra conta.

# Subtraindo


![](/assets/img/curso_logica_src/explicacao_subtracao_binario.png)

Aqui é do mesmo jeito, seguimos a mesma regra da subtração que conhecemos.

Na coluna 0, temos 0 – 1, isto é uma operação impossível, não tem o que tirar de zero, então, “pedimos emprestado” do número a seguir, que no caso é 1, ou seja, 10 – 1 = 1.

Na coluna 1, teria 1 – 0, mas lembra que pegamos 1 emprestado? Então, o 1 que tá ai em cima, agora vira 0. Ou seja, agora fica: 0 – 0 = 0

Na coluna 2, temos 1 – 0, que é 1

E por fim, na coluna 3, temos 1 – 1, que é 0.

E assim, fechamos a conta.

Viu como é fácil? Experimente fazer umas contas simples assim!

E bom, por este artigo é só, espero muuuuito que você tenha aproveitado! Até a próxima! C:
