---
layout: post
title: Criando uma distro linux parte 2
description: Segunda parte do post "criando uma distribuição linux"
categories: Linux
---

No primeiro post deste blogue, eu falei um pouco sobre criação de distros, e como uma distro funciona, e também disse que já tinha dado uma olhada mais a fundo, pois bem, agora eu vou compartilhar o que eu descobri. :)

Bom, eu dei uma explorada dentro da ISO de live cd do Debian XFCE, pra extrair é fácil, o Nemo do Cinnamon consegue. E usando o comando `ls` pra ver o que tem dentro dela, temos isto:

```shell
$ ls
boot      dists  efi.img     install      live               pool           sha256sum.README  tools
debian  EFI    firmware  isolinux  md5sum.txt  pool-udeb  sha256sum.txt
```
Ok, um monte de checksuns, uma pasta onde fica o bootloader, e a pasta live, provavelmente ela vai ter o ambiente que roda no live cd, abrindo…

```shell
$ cd live
$ ls
filesystem.packages  filesystem.squashfs  initrd.img  _initrd.img.extracted  vmlinuz  vmlinuz-6.1.0-18-amd64
```
Legal! Temos um arquivo “filesystem.squashfs”. squashfs é literalmente um arquivo que comprime um filesystem de Linux, e isso quer dizer, que ele pode ser montado.

```shell
# mount --type="squashfs" --options="loop" --source="filesystem.squashfs" /mnt/debian
# cd /mnt/debian
# ls
bin  boot  dev  etc  home  initrd.img  lib  lib64  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var  vmlinuz
```

Olha só! Temos a raiz de um sistema Linux :0

E se usarmos o comando “chroot” aqui dentro?

Opa! O chroot até funciona, mas o squashfs por padrão é só de readonly, então não dá pra usar o apt pra baixar coisas.

Tem uma forma de resolver isso, no meu OS principal (linux mint) vou instalar o pacote “squashfs-tools”. Com ele eu posso fazer o seguinte:

```shell
$ unsquashfs filesystem.squashfs
$ mv squashfs-root/* /mnt/debian
$ cd /mnt/
$ sudo chroot debian
#
```

Bom, isto deveria funcionar, mas ainda tem alguns problemas com os repositórios de debian. Se eu bem me lembro da minha instalação do gentoo, a gente precisava montar algumas coisas antes de dar chroot.

Depois de uma boa pesquisada, encontrei um post no stack overflow sobre como fazer chroot apropriadamente, e falava sobre montar as partições que eu falei antes proc, sys, run…

mas isto também não funcionou, dando outra pesquisada, descobri que aparentemente tem um jeito de montar squashfs em read-write, xô vê…

Humm isto também não funciona…

Bom, por agora, eu vou deixar assim.

Mas é claro, eu já dei meu jeitinho de editar essa ISO, no próximo post eu explico como, até lá!
