---
layout: post
title: Um projeto legal que tô fazendo :3 
description: Só mostrando o desenvolvimento de um projeto meu que está em desenvolvimento a alguns dias, espero que gostem :3
categories: Linux
---

Faz muito tempo que eu faço um post aqui, motivo? tô preso em um projeto pessoal meu, essa bodega tá tomando toda a minha atenção, e tirando o meu sono, mas tudo bem, é assim que eu funciono :P

Bem, chega de enrrolação, o que eu tô fazendo, é basicamente... poxa! nem eu sei o que é isso T-T
Saca o Medium? ou um Writefreely, é tipo isso, só que open source (diferente do Medium), e não tem integração com o ActivityPub (que nem o Writefreely, pelo menos por enquanto). Ele já é funcional em questão de escrever e publicar artigos em blogues, mas ainda não tem toda aquela parte de estilização e tudo mais.

Ainda falta algumas coisinhas pra fazer, e muito código pra reescrever, já que eu sou um iniciante em Ruby on Rails e não paro de aprender uma forma melhor de fazer uma coisa. Inclusive, Rails é algo que eu gostaria de falar aqui.

Eu tirei uns prints para vocês verem ele, tá tudo feioo, eu só mexi um pouquinho na página inicial e na página de login, o resto é basicamente um html puro com fundo preto.

![Página inicial](/assets/img/post_src/pagina_inicial.png)
O nome Capyblog é temporario, mas provavelmente vai ser isso mesmo, é uma mistura de capybara (capivara em "gringo"), e, blog (blogue em "gringo")

Bom, esta é a página inicial, dá pra ver que parece muito com o layout do meu blogue, é, eu gosto bastante disso :)

Eu não pus uma barra de navegação, como vocês podem perceber. E não que eu tenha problema com barras de navegação, mas, quis fazer diferente, só isso :P

Nessa página você vai encontrar os blogues recém criados, e os posts publicados, ainda vou encontrar uma forma melhor de mostrar eles, mas por enquanto, isto deve servir

![Página de escrita de post](/assets/img/post_src/pagina_de_escrita_de_post.png)

Essa coisa horrorosa aí, é onde você vai escrever os posts, não se preocupe, vai ainda ficar muito melhor que isso!

Fica fácil perceber que você pode usar markdown aí, e isso não é necessariamente uma novidade, qualquer site assim permite fazer algo parecido.

![Página do post](/assets/img/post_src/pagina_do_post.png)

E também fica fácil perceber que eu não sou bom em demonstrar coisas :v

As listas estão funcionais sim, aí parece texto puro, não sei o que houve, acho que ficou assim quando eu tirei as margens dos elementos, tanto faz, depois eu arrumo :v

Os links em baixo, só vão aparecer se você for o criador do post, óbvio. Com eles, você pode editar ou deletar um post, óbviviamente

![Página do blogue](/assets/img/post_src/pagina_do_blog.png)

A pagina dos blogues está assim, crua e sem estilo, como eu disse, vou dar uma arrumada em tudo depois.

Aí você vai encontrar uma lista de posts recentes e algumas informações sobre o blogue, ainda vai ganhar páginas personalizadas, pra os usuários poderem criar páginas de sobre, rolo, e tudo mais.

### Concluindo
Como eu falei, tá tudo em desenvolvimento, talvez eu siga o estilo da página inicial, tá gostozinho e legal o suficiente, tem muitas funcionalidades pra por, e, se você for rubista e quiser me ajudar, logo logo eu vou por o link do repositório. E vou botar umas caixinhas com as coisas que estou fazendo, e que falta fazer, assim vocês podem me ajudar a desenvolver, e terminar mais rápido :)

O projeto vai ser hospedado em uma vps que for mais barata, não é nada muito gigante, então não vou precisar de muito espaço de armazenamento, só um bom processamento e RAM.

Quem tiver sugestões de serviços, por favor, me avizem (links de contato na página social) :3

Bom, espero que vocês estejam ansiosos pra recebe-lo, o site vai ser gratuito, e vou mante-lo vivo com doações, não acho que vai ser um investimento muito grande, no máximo uns 50~60 reais mensais (vps, e domínio), não seria muito difícil conseguir isso se eu receber doações em dolar.

Até a próxima, e... valeu! :3
