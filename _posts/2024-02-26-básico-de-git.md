---
layout: post
Title: O básico do básico de git
description: Um post especial pra um amigo próximo com dúvidas de como git funciona x)
categories: Programação
---

Bom, pulando a parte chata da história do git...

O git é um versionador de código, então, vamos versionar uns códigos? :)

Antes de mais nada, você precisa adicionar seu email e nome no git, faz assim:

```shell
$ git config --global user.name = "seu lindo nome"
$ git config --global user.email = "seu lindo email"
```

Não precisa fazer verificação de email que normalmente você faria em um software comum, então relaxe ;)

Agora, vamos com o tutorial :)

Olha só, abrindo qualquer diretório no seu sistema de digitando `git init` vai iniciar um repositório novo e vazio.
Ele também serve caso você já tenha código em um gitlab ou github da vida, depois eu explico como!

Agora que temos um repositório git, podemos adicionar uns arquivos...

```shell
$ touch helloworld.txt
$ echo "Olá mundo!" >> helloworld.txt
$ cat helloworld.txt
Olá mundo!
```

Beleza! temos um arquivo! :)

se usarmos o comando `git status`, o git vai nos avizar que o arquivo está "unstaged", isso quer dizer que ele não faz parte do repositório, o git por padrão, marca os arquivos novos como "unstaged", e esses não vão ser versionados. Para arrumar isso, podemos usar o comando: `git add <nome do arquivo>`, se você quiser adicionar mais, é só fazer: `git add <arquivo1> <arquivo2>`, ou simplesmente: `git add .`, isso vai adicionar todos os arquivos ao repositório :)

Pronto, agora podemos criar a primeira versão do nosso código, e é muito simples! :3

```shell
$ git commit -m "mensagem descritiva que explica o que você fez nessa versão"
```

E pronto! temos uma nova versão do nosso código! vamos adicionar mais conteúdo ao arquivo...

```shell
$ echo "Olá mundo 2" >> helloworld.txt
$ cat helloworld.txt
Olá mundo
Olá mundo 2
```

E então, criamos uma nova versão...

```shell
$ git commit -m "adicionei mais um olá mundo..."
```

Se usarmos o comando `git log`, o git vai nos mostrar todos os commits que criamos, junto com suas respectivas hashs (um monte do número e letra, é uma hash de sha1, mas o importante é você saber que vamos usar ela pra identificar os commits).

Digamos que a nova versão do código que criamos deu bug, PUTS! e agora? Em um código normal, você teria que ir apagando e recolocando as linhas que apagou, ou usando ctrl+z, mas se você foi esperto e deu muitos commits constantemente, é fácil voltar a versão que o código estava funcionando!

```shell
$ git reset --hard 12345
$ cat helloworld.txt
Olá mundo
```

Com o comando `git reset --hard`, você pode passar a hash do commit que você quer voltar, e pronto! você tem o código do jeito que tava quando você fez esse commit. Sim, a hash do git é gigante, mas pra facilitar pra você, o git te permite passar como parâmetro só uma parte da hash, nós programadores costumamos usar os primeiros 5 dígitos.

E bom, este é o básico do básico de git, ainda faltam as branchs, mas nada que uma pesquisada no "pato pato vai" não resolva :3

Agora, vamos mandar esse código pra um servidor de git? :3

Independente de qual você usar, o processo é basicamente o mesmo, e antes de mais nada, você precisa criar um par de chaves ssh para criptografia, e é muito simples! :D 

```shell
$ ssh-keygen -t ed25519 -C "seu-lindo-email@email.com"
```

Ele vai te perguntar várias coisas, não precisa fazer nada além de apertar enter até finalizar.

Com isso feito, você deve ver um novo diretório oculto na home do seu sistema, com o nome ".ssh".

se entrarmos nele, vai ter dois arquivos: "alguma coisa" e "alguma coisa.pub", esse é o seu par de chaves, e "alguma coisa", é a chave privada, e "alguma coisa.pub" vai ser a sua chave pública.

Lembre-se, a chave se chama "privada" por um motivo (e não tem nada haver com banheiro), ela não pode entrar na internet de forma ALGUMA, se isso acontecer, delete as chaves e crie novas. A chave pública tudo bem, ela não é exatamente pública, mas não tem problema se vazar na internet.

Agora, podemos mandar a chave pública para o nosso servidor, todos ele devem ter uma sessão nas configurações com algo tipo "ssh/gpg keys", abre ela.

Agora, com o seu lindo terminal, vê a sua chave pública.

```shell
$ cat ~/.shh/alguma coisa.pub
Aqui vai retornar um monte de coisa, tudo isso é a sua chave ssh
```

Na página do seu servidor git, na sessão de chaves ssh, vai ter dois campos, um grandão que vai pedir sua chave ssh, e outro menor que vai pedir o nome que você quer dar pra sua chave, pode ser qualquer um, não importa. Talvez ele peça uma data de expiração, coloca aí uns 2 anos, pra você não se preocupar com isso ;)

E pronto, cadastre sua chave. Agora podemos nos comunicar com o servidor! :D

Se você já tem um repositório no servidor, você agora vai poder clonar ele com a opção "ssh", copia o link que ele vai dar, e usa esse comando:

```shell
$ git clone link-ssh-do-repositório
```

E claro, garanta que você escolheu a URL de ssh, se não você não vai conseguir enviar as coisas pro repositório.

Entre dentro (entre dentro hehehehe) do seu repositório, modifique uns arquivos, faça commit, e agora, você pode usar este comando:

```
$ git push
```
Ele vai te pedir pra confirmar se quer adicionar o seu servidor de git aos "know_hosts", só digita "yes" e aperta "Enter", ele não vai mais de incomodar ;)
Só com isso, você vai ser capaz de mandar suas alterações para o repositório remoto, se por acaso, o repositório remoto estiver mais atualizado que o seu (aconteçe normalmente caso estiver trabalhando em equipe), use o comando:

```shell
$ git pull
```

Isso vai "puxar" os arquivos do repositório remoto.

Ok, é assim que você trabalharia usando git, de forma _BÁSICA_, existem diversos workflows com git que são interessantes pra você não perder muito tempo, mas no geral é só isso ;)

Bom, se você acabou de criar um repositório remoto, e todos os seus arquivos estão em um repositório local (na sua máquina) então faça o seguinte:

```shell
$ git remote add origin <link ssh do repositório remoto>
$ git pull
$ git push
```

Provavelmente vai dar uns errinhos aí, mas o git é legal e vai te gerar comandos que você vai copiar e colar, e vai resolver.

Bom, pra esse post é isso, dúvidas? me chama lá no mastodon.

Até mais! :3
